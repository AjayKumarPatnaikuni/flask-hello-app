from flask import Flask
app = Flask(__name__)

@app.route('/Deploy')
def hello():
    return "Deploying flask app by using jenkins bitbucket integration"
    
@app.route('/')
def helloworld():
    return "Hello World! from Flask"

@app.route('/health-status')
def health():
    return "Application up and running successfully"


if __name__ == "__main__":
    app.run(host ='0.0.0.0', port = 5000)